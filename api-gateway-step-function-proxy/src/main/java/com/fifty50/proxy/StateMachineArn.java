package com.fifty50.proxy;

public enum StateMachineArn {
    /**
     * NOTE: for adding new entries, make sure you put equivalent of Resource set in API Gateway and replace slashes with
     * underscores and then put HttpMethod at the end with another underscore prefix
     * e.g. for /user/initialization POST method, it'll be: USER_INITIALIZATION_POST
     */
    USER_GET("arn:aws:states:us-east-2:782143074122:stateMachine:user-retrieval-flow"),
    USER_INITIALIZATION_POST("arn:aws:states:us-east-2:782143074122:stateMachine:user-registration-flow");

    private final String stateMachineArn;

    StateMachineArn(String stateMachineArn) {
        this.stateMachineArn = stateMachineArn;
    }

    public static String getStateMachineArn(final ApiGatewayEndpointDto apiGatewayEndpoint) {
        String suffix = apiGatewayEndpoint.getHttpMethod().replace("/", "_");
        String prefix = apiGatewayEndpoint.getResource().substring(1).replace("/", "_").toUpperCase();

        // TODO: We probably need proper error handling between us and client.
        return valueOf(prefix + "_" + suffix).getStateMachineArn();
    }

    public String getStateMachineArn() {
        return stateMachineArn;
    }
}
