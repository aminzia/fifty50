package com.fifty50.proxy;

import java.util.Map;

public class ApiGatewayEndpointDto {
    private String resource;
    private String httpMethod;
    private String body;
    private Map<String, Object> queryStringParameters;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, Object> getQueryStringParameters() {
        return queryStringParameters;
    }

    public void setQueryStringParameters(Map<String, Object> queryStringParameters) {
        this.queryStringParameters = queryStringParameters;
    }
}
