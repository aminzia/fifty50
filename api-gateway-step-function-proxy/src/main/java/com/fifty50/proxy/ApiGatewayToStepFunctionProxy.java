package com.fifty50.proxy;

import com.amazonaws.ResponseMetadata;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public class ApiGatewayToStepFunctionProxy implements RequestStreamHandler {
    private AWSStepFunctions awsStepFunctions = AWSStepFunctionsClientBuilder.defaultClient();
    private static final Gson gson = new GsonBuilder().create();

    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        ApiGatewayEndpointDto apiGatewayEndpoint = gson.fromJson(reader, ApiGatewayEndpointDto.class);

        StartExecutionRequest startExecutionRequest = getStartExecutionRequest(apiGatewayEndpoint, logger);
        StartExecutionResult startExecutionResult = awsStepFunctions.startExecution(startExecutionRequest);
        ResponseMetadata sdkResponseMetadata = startExecutionResult.getSdkResponseMetadata();
        logger.log(gson.toJson(sdkResponseMetadata));
    }

    private StartExecutionRequest getStartExecutionRequest(ApiGatewayEndpointDto apiGatewayEndpoint, LambdaLogger logger) {
        logger.log("Received a request for API: " + apiGatewayEndpoint.getResource() + " with HTTP method " + apiGatewayEndpoint.getHttpMethod());
        logger.log("body: " + apiGatewayEndpoint.getBody());
        StartExecutionRequest startExecutionRequest = new StartExecutionRequest();

        String lambdaInput = getLambdaInput(apiGatewayEndpoint);
        logger.log("Received these query string params: " + lambdaInput);
        startExecutionRequest.setInput(lambdaInput);

        startExecutionRequest.setStateMachineArn(StateMachineArn.getStateMachineArn(apiGatewayEndpoint));
        return startExecutionRequest;
    }

    private String getLambdaInput(ApiGatewayEndpointDto apiGatewayEndpoint) {
        if (apiGatewayEndpoint.getHttpMethod().equals("GET")) {
            return gson.toJson(apiGatewayEndpoint.getQueryStringParameters());
        }
        return apiGatewayEndpoint.getBody();
    }
}
