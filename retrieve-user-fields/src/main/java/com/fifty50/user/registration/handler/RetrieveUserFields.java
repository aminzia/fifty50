package com.fifty50.user.registration.handler;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;
import com.fifty50.domain.UsersFile;

public class RetrieveUserFields implements RequestHandler<UsersFile, UsersFile> {

    private static final AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    private static final DynamoDBMapper mapper = new DynamoDBMapper(client);

    @Override
    public UsersFile handleRequest(UsersFile input, Context context) {
        if (input == null || StringUtils.isNullOrEmpty(input.getEmailAddress())) {
            throw new IllegalArgumentException("No email was passed. What's happening Kenny?");
        }
        UsersFile retrievedUser = mapper.load(UsersFile.class, input.getEmailAddress());

        if (retrievedUser == null) {
            retrievedUser = new UsersFile();
            retrievedUser.setEmailAddress(input.getEmailAddress());
            retrievedUser.setUserFound(false);
        }

        return retrievedUser;
    }
}
