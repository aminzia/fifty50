package com.fifty50.domain;

@Deprecated
public enum LambdaFunction {
    INITIALIZE_USER_REGISTRATION("InitializeUserRegistration",UsersFile.class, Boolean.class),
    RETRIEVE_USER_FIELDS("RetrieveUserFields", UsersFile.class, UsersFile.class);

    private final String name;
    private final Class<?> input;
    private final Class<?> output;

    private LambdaFunction(String name, Class<?> input, Class<?> output) {
        this.name = name;
        this.input = input;
        this.output = output;
    }

    public String getName() {
        return name;
    }

    public Class<?> getInput() {
        return input;
    }

    public Class<?> getOutput() {
        return output;
    }
}
