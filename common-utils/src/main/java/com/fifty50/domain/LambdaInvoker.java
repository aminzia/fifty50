package com.fifty50.domain;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

@Deprecated
public class LambdaInvoker {
    private static final AWSLambda lambdaClientBuilder = AWSLambdaClientBuilder.standard().withRegion(Regions.US_EAST_2).build();
    private static final Gson gson = new GsonBuilder().create();

    public static void runWithoutPayloadSync(LambdaFunction lambdaFunction) {
//        runWithPayloadSync(lambdaFunction, null);
    }

    public static <T, U> U runWithPayloadSync(Class<?> clazz, T payload) {
        // TODO: do we need this?
//        String payloadJson = payload != null ? gson.toJson(payload) : "";
//
//        InvokeRequest request = new InvokeRequest();
//        request.withFunctionName(lambdaFunction.getName()).withPayload(payloadJson);
//        InvokeResult result = lambdaClientBuilder.invoke(request);
//
//        String resultPayload = "";
//        if (result.getPayload() != null) {
//            resultPayload = new String(result.getPayload().array(), Charset.forName("UTF-8"));
//        }
//
//        System.out.println("Result invoking " + lambdaFunction + ": " + resultPayload);
        return null;
    }

    public static Class<?> getRequestHandlerInputType(Class<?> clazz) {
        //TODO: add logic for returning Input and output type for the function.
        for (Type parameterizedType : clazz.getGenericInterfaces()) {
            String typeName = parameterizedType.getTypeName();
            if (typeName.contains("RequestHandler")) {
                return (Class<?>) ((ParameterizedType) parameterizedType).getActualTypeArguments()[0];
            }
        }

        return null;
    }
}
