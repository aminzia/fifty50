package com.fifty50.domain;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

import java.util.Map;

public class Util {

    public static void printShit(Map<String, Object> someMap, LambdaLogger logger) {
        if (someMap != null) {
            for (Map.Entry<String, Object> entrySet : someMap.entrySet()) {
                logger.log("printing query string params: key: " + entrySet.getKey() + " value: " + entrySet.getValue().toString());
            }
        }
    }
}
