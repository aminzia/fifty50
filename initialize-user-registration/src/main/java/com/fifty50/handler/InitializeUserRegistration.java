package com.fifty50.handler;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;
import com.fifty50.domain.UsersFile;

public class InitializeUserRegistration implements RequestHandler<UsersFile, Boolean> {

    private static final AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    private static final DynamoDBMapper mapper = new DynamoDBMapper(client);

    public Boolean handleRequest(UsersFile usersFile, Context context) {
        if (usersFile == null || StringUtils.isNullOrEmpty(usersFile.getEmailAddress()))
            throw new IllegalArgumentException("No email was passed. What's happening Kenny?");

        mapper.save(usersFile);
        return true;
    }
}
